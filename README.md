# YASH
Yet Another Smart Home

Smart home management system base on Meteor js

Usage:
========
- Install [meteor js](https://www.meteor.com/)
- Clone YASH: `git clone https://github.com/dangsonbk/YASH`
- Run: cd to YASH application folder and run `meteor`

Packages:
========
[meteor-uploads](https://github.com/tomitrescak/meteor-uploads)
[momentjs](http://momentjs.com)