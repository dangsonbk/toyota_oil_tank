toy_scale_realtime = new Mongo.Collection("toy_scale_realtime");
toy_scale_timely = new Mongo.Collection("toy_scale_timely");
toy_scale_quota = new Mongo.Collection("toy_scale_quota");

Meteor.subscribe("toyScaleRealtime");
Meteor.subscribe("toyScaleTimely");
Meteor.subscribe("toyScaleQuota");

var buff = ""
// This code only runs on the client
Template.toy_scale.helpers({
  scale_data_realtime: function () {
    data = toy_scale_realtime.findOne({});
    if (data["data"] != buff)
    {
        console.log(data["data"]);
        Meteor.call("toyUpdateData", data["data"]);
    }
    buff = data["data"];
    return data;
  },
  toyDataTimely: function () {
    return toy_scale_timely.find({});
  },
  toyDataCountToday: function () {
    return toy_scale_timely.find({}).count();
  },
  toy_scale_quotas: function () {
    return toy_scale_quota.find({});
  }
});

Template.toy_scale.events({
  "submit .toyAddQuota": function(event) {
    event.preventDefault();
    // Get value from form element
    var data = {};
    data["code"] = event.target.toy_quota_code.value;
    data["quota"] = event.target.toy_quota_quota.value;
    // Insert a task into the collection
    Meteor.call("toyInsertQuota", data);

    // Clear form
    event.target.toy_quota_code.value = "";
    event.target.toy_quota_quota.value = "";
  },

  "submit .toyExportXls": function(event) {
    event.preventDefault();
    // Get value from form element
    var data = {};
    data["start"] = event.target.toy_export_startDate.value;
    data["end"] = event.target.toy_export_endDate.value;
    // Insert a task into the collection
    //Meteor.call("toyExportData", data);
    console.log(data);

    // Clear form
    event.target.toy_export_startDate.value = "";
    event.target.toy_export_endDate.value = "";
  },

  "onchange #current_volume": function(event) {
    event.preventDefault();
    console.log("data_change");
  }
});

Template.toy_scale_quota.events({
  "click .toyRemoveQuota": function(event) {
    Meteor.call("toyDeleteQuota", this._id);
  }
});

Template.toy_scale.rendered= function() {
    $('#datepicker-start').datepicker();
    $('#datepicker-end').datepicker();
}