yash_devices = new Mongo.Collection("yash_devices");

Meteor.subscribe("yashDevices");

Template.yash_device.helpers({
  yashDevices: function(){
    return yash_devices.find({},{limit:10,skip:0});
  },
})

Template.yash_device.events({
  "submit .yashDeviceAdd": function(event) {
    // Prevent default browser form submit
    event.preventDefault();
    // Get value from form element
    var yash_device_name = event.target.yashDeviceAddName.value;
    var yash_device_type = event.target.yashDeviceAddType.value;
    // Insert a task into the collection
    Meteor.call("yashDeviceAdd", yash_device_name, yash_device_type);

    // Clear form
    event.target.yashDeviceAddName.value = "";
    event.target.yashDeviceAddType.value = "";
  },
})

Template.yash_device_each.events({
  "click .yashDeviceDelete": function () {
    Meteor.call("yashDeviceDelete", this._id);
  },

  "click .yashTodoToggleChecked": function () {
    // Set the checked property to the opposite of its current value
    //Meteor.call("yashTodoSetChecked", this._id, ! this.checked);
  },

  "click .yashTodoSetPrivate": function () {
    //Meteor.call("yashTodoSetPrivate", this._id, ! this.private);
  },

  "click .yashTodoEdit": function () {
    alert("Tính năng chưa làm, thông cảm, muốn sửa vui lòng xóa đi rồi tạo mới, pro thì tự viết tính năng. rofl");
  }
})