yash_todo_tasks = new Mongo.Collection("yash_todo_tasks");

Meteor.subscribe("yashTodoElements");

Template.yash_todo.helpers({
  yashTodoElements: function(){
    return yash_todo_tasks.find({},{limit:10,skip:0});
  },
  yashTodoPageList: function(){
    var pages = [1,2,3,4];
    return pages;//yash_todo_tasks.find({}).count();
  }
})

Template.yash_todo.events({
  "submit .yashTodoAddTask": function(event) {
    // Prevent default browser form submit
    event.preventDefault();
    // Get value from form element
    var yash_todo_element = event.target.yashTodoAddTaskTxt.value;
    // Insert a task into the collection
    Meteor.call("yashTodoAddTask", yash_todo_element);

    // Clear form
    event.target.yashTodoAddTaskTxt.value = "";
  },
})

Template.yash_todo_element.events({
  "click .yashTodoRemoveTask": function () {
    Meteor.call("yashTodoDeleteTask", this._id);
  },

  "click .yashTodoToggleChecked": function () {
    // Set the checked property to the opposite of its current value
    Meteor.call("yashTodoSetChecked", this._id, ! this.checked);
  },

  "click .yashTodoSetPrivate": function () {
    Meteor.call("yashTodoSetPrivate", this._id, ! this.private);
  },

  "click .yashTodoEdit": function () {
    alert("Tính năng chưa làm, thông cảm, muốn sửa vui lòng xóa đi rồi tạo mới, pro thì tự viết tính năng. rofl");
  }
})