Router.configure({
  layoutTemplate: 'main',
});

Router.route('/', {
  name: 'home',
  template: 'toy_scale',
});

Router.route('/device', {
  name: 'device',
  template: 'yash_device',
});

Router.route('/scale', {
  name: 'scale',
  template: 'toy_scale',
});

// given a url like "/post/5"
Router.route('/scale/:data', {
  action: function() {
    Meteor.call("toyUpdateData", this.params.data);
  },
  name: "scale_data",
  layoutTemplate: 'null',
});

Router.route('/register', {
  name: 'register',
  template: 'register',
  layoutTemplate: 'plain',
});

Router.route('/login', {
  name: 'login',
  template: 'login',
  layoutTemplate: 'plain',
});

Router.onBeforeAction(function () {
  if (!Meteor.user() && !Meteor.loggingIn()) {
      this.redirect('/login');
  } else {
      // required by Iron to process the route handler
      this.next();
  }
}, {
    except: ['login','register','scale_data']
});
