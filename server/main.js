Meteor.startup(function () {
  UploadServer.init({
    tmpDir: process.cwd() + '/uploads/tmp',
    uploadDir: process.cwd() + '/uploads',
    checkCreateDirectories: true, //create the directories for you
    finished: function(fileInfo, formFields) {
      console.log( fileInfo );
      var xlsx_url = process.cwd() + '/uploads/' +fileInfo["name"];
      Meteor.call("consoleimportXLS", 'E:\\toy_scripts\\xlsimport.py "' + xlsx_url + '"');
    },
  },);
});