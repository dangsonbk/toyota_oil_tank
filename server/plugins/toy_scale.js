toy_scale_realtime = new Mongo.Collection("toy_scale_realtime");
toy_scale_timely = new Mongo.Collection("toy_scale_timely");
toy_scale_quota = new Mongo.Collection("toy_scale_quota");

exec = Npm.require('child_process').exec;
Fiber = Npm.require('fibers');

_inputXlsx = function(cmd, stdoutHandler, stderrHandler) {
  exec(cmd, Meteor.bindEnvironment(
      function(error, stdout, stderr) {
        if (stdout != "")
            stdoutHandler(stdout);
        if (stderr != "")
            stderrHandler(stderr);
      }
    )
  );
}

// Import json to db
importToDB = function(_data) {
  var _dataObj = JSON.parse(_data);
  var insert_entry = {};
  _dataObj.forEach( function(entry) {
    insert_entry["time"] = new Date(entry["time"]);
    insert_entry["code"] = entry["code"];
    insert_entry["name"] = entry["name"];
    var quota = toy_scale_quota.findOne({code: insert_entry["code"]});
    if(quota != null){
      insert_entry["quota"] = quota["quota"];
    }
    else
    {
      insert_entry["quota"] = "0";
    }
    insert_entry["current"] = "-";
    toy_scale_timely.insert(insert_entry);
  });
}

stderrHandler = function(_data) {
  console.log(_data);
}

Meteor.publish("toyScaleRealtime", function () {
  return toy_scale_realtime.find();
});

Meteor.publish("toyScaleQuota", function () {
  return toy_scale_quota.find();
});

Meteor.publish("toyScaleTimely", function () {
  var today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  return toy_scale_timely.find({"time": { $gte: today}}, {sort: {"time": 1}});
});

Meteor.methods({
  toyUpdateData: function (data) {
    var db_data = toy_scale_realtime.find({});
    var now = new Date();
    if(db_data.count() == 1)
    {
      // toy_scale_realtime.update({},{data:data, updatedAt: now});

      // find current time stack and previous time stack
      var current_stack = toy_scale_timely.findOne({"time": { $lte: now}}, {sort: {"time": -1}, limit: 1});
      var prev_stack = toy_scale_timely.findOne({"time": { $lt: current_stack["time"]}}, {sort: {"time": -1}, limit: 1});

      // calculate actual and diff
      if(prev_stack["current"] == "-")
      {
        toy_scale_timely.update({"_id": current_stack["_id"]}, {$set:{current: data}})
      }
      else
      {
        var actual_use = parseFloat(prev_stack["current"]) - parseFloat(data);
        var diff = actual_use - parseFloat(current_stack["quota"]);
        toy_scale_timely.update({"_id": current_stack["_id"]}, {$set:{current: data, actual:actual_use, diff: diff}})
      }
    }
  },

  consoleimportXLS : function(cmd) {
    _inputXlsx(cmd, importToDB, stderrHandler);
  },

  toyInsertQuota: function (data) {
    toy_scale_quota.insert({code:data["code"], quota:data["quota"]});
  },

  toyDeleteQuota: function (_id) {
    toy_scale_quota.remove({_id:_id});
  }

});