yash_devices = new Mongo.Collection("yash_devices");

Meteor.publish("yashDevices", function () {
  return yash_devices.find({});
});

Meteor.methods({
  yashDeviceAdd: function (name, type) {
    yash_devices.insert({
      name: name,
      type: type,
      status: "fresh",
      createdAt: new Date()
    });
  },

  yashDeviceDelete: function (deviceId) {
    var device = yash_devices.findOne(deviceId);
    yash_devices.remove(deviceId);
  }
});