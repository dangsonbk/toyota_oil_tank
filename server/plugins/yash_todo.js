yash_todo_tasks = new Mongo.Collection("yash_todo_tasks");

Meteor.publish("yashTodoElements", function () {
  return yash_todo_tasks.find({
    $or: [
        { private: {$ne: true} },
        { owner: this.userId }
      ]
  });
});

Meteor.methods({
  yashTodoAddTask: function (yash_todo_element) {
    yash_todo_tasks.insert({
      yashTodoElement: yash_todo_element,
      createdAt: new Date(),
      owner: Meteor.userId(),
      username: Meteor.user().username,
      private: true
    });
  },

  yashTodoDeleteTask: function (taskId) {
    var task = yash_todo_tasks.findOne(taskId);
    if (task.private && task.owner !== Meteor.userId()) {
      // If the task is private, make sure only the owner can delete it
      throw new Meteor.Error("not-authorized");
    }

    yash_todo_tasks.remove(taskId);
  },

  yashTodoSetChecked: function (taskId, setChecked) {
    var task = yash_todo_tasks.findOne(taskId);
    if (task.private && task.owner !== Meteor.userId()) {
      // If the task is private, make sure only the owner can check it off
      throw new Meteor.Error("not-authorized");
    }

    yash_todo_tasks.update(taskId, { $set: { checked: setChecked} });
  },

  yashTodoSetPrivate: function (taskId, setToPrivate) {
    var task = yash_todo_tasks.findOne(taskId);

    // Make sure only the task owner can make a task private
    if (task.owner == Meteor.userId()) {
        yash_todo_tasks.update(taskId, { $set: { private: setToPrivate } });
    }

  }
});